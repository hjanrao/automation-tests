package practice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;

public class test {

    public static void main(String[] args) {

        Enumeration names;
        Collection<String> names1;
        String key;

        // Creating a Hashtable
        Hashtable<String, String> hashtable = new Hashtable<String, String>(2);


        // Adding Key and Value pairs to Hashtable
        hashtable.put("Key1", "Chaitanya");
        hashtable.put("Key2", "Ajeet");
        hashtable.put("Key3", "Peter");
        hashtable.put("Key4", "Ricky");
        hashtable.put("Key5", "Mona");

        names = hashtable.keys();
        names1 = hashtable.values();

        while (names.hasMoreElements()) {
            key = (String) names.nextElement();
            System.out.println("Key: " + key + " & Value: " +
                    hashtable.get(key));
        }


        System.out.println("Values are "+ hashtable.toString());
    }
}