package tests.ui.login;

import org.testng.Assert;
import org.testng.annotations.*;
import test.automation.framework.api.annotations.Groups;
import test.automation.framework.api.annotations.TestID;
import test.automation.framework.test.base.ui.BaseUITest;
import tests.ui.pages.login.CustomerLoginPage;

public class LoginTests extends BaseUITest
{

	private CustomerLoginPage custPage;

	@BeforeClass(alwaysRun=true)
	public void beforeClass()
	{
		custPage = new CustomerLoginPage(wd).navigateTo();
	}

	@AfterMethod()
	public void afterTest()
	{
		System.out.println("After test");
	}

	@AfterClass()
	public void afterTests()
	{
		wd.quit();
	}

	@TestID("d4c28098-fbbb-4b0a-8900-b22de8933a71")
	@Test(groups= {Groups.CATEGORY_SANITY})
	public void validLogin(){
		custPage.validLogin("hemantjanrao@gmail.com", "@Test1234");

		Assert.assertTrue(wd.getCurrentUrl().contains("/index.php?controller=my-account"));
	}

    @TestID("d4c28098-fbbb-4b0a-8900-b22de8933a71")
    @Test(groups= {Groups.CATEGORY_SANITY})
    public void invalidLogin(){
        custPage.validLogin("hemantjanrao@gmail.com", "@Test1");

        Assert.assertTrue(custPage.isAuthFailed());
    }
}
