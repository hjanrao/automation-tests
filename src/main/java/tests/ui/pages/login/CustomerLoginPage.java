package tests.ui.pages.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import test.automation.framework.ui.page.BasePage;
import test.automation.framework.ui.utils.WebUtils;

public class CustomerLoginPage extends BasePage<CustomerLoginPage> {

    private final String url = "/index.php?controller=authentication&back=my-account";

    public CustomerLoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getURL() {
        return url;
    }

    @FindBy(id="email")
    private WebElement txtbxEmail;

    @FindBy(id="passwd")
    private WebElement txtbxPasswd;

    @FindBy(id="SubmitLogin")
    private WebElement btnSubmitLogin;

    @FindBy(css = ".alert.alert-danger")
    private WebElement lblAuthFailed;

    public void validLogin(String userName, String passWd){

        WebUtils.waitForPageLoad(wd);

        WebUtils.fill(txtbxEmail,userName);
        WebUtils.fill(txtbxPasswd,passWd);

        WebUtils.clickWithWaitForElement(wd, btnSubmitLogin);
    }

    public boolean isAuthFailed(){
        WebUtils.waitForElementToBeDisplayed(wd, lblAuthFailed,50);
        return WebUtils.isElementDisplayed(lblAuthFailed);
    }
}
