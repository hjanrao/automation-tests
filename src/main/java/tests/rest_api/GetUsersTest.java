package tests.rest_api;

import io.restassured.response.ValidatableResponse;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import test.automation.framework.api.annotations.Groups;
import test.automation.framework.api.annotations.TestID;
import test.automation.framework.restAPI.services.GetUsers;
import test.automation.framework.test.base.api.BaseTestApi;


public class GetUsersTest extends BaseTestApi
{
    GetUsers gu =null;

    @BeforeClass
    public void beforeTestClass()
    {
        gu = new GetUsers();
    }

    @TestID("1893fa2e-dd35-4f36-a988-a2f6b04bc8f4")
    @Test(groups= Groups.CATEGORY_SANITY)
    public void verifyOrderManagement()
    {
        ValidatableResponse users = gu.getUsers();

        System.out.println(users.extract().response().prettyPrint());

        System.out.println("data is "+ users.extract().jsonPath().get("data.0"));

    }
}